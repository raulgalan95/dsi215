package com.sisbam.sgp.controller.administration;

import java.util.ArrayList;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.gson.Gson;
import com.sisbam.sgp.controller.variety.ObtenerPermisosPorUrl;
import com.sisbam.sgp.dao.DaoImp;
import com.sisbam.sgp.entity.administration.Empleado;
import com.sisbam.sgp.entity.administration.Proyecto;
import com.sisbam.sgp.entity.administration.Solicitud;
import com.sisbam.sgp.entity.administration.Subcategoria;
import com.sisbam.sgp.entity.administration.TipoProyecto;
import com.sisbam.sgp.entity.administration.Usuario;
import com.sisbam.sgp.entity.security.Permisos;
import com.sisbam.sgp.entity.security.Rol;

@Controller
public class GraficosController {
	@Autowired
	private DaoImp manage_entity;
	
	private String path = "Administration/Graficos/";
	private static final String IDENTIFICADOR = "graficoS";
	
	private Permisos permisos;
	
	
	@RequestMapping(value = "/graficos", method = RequestMethod.GET)
	 public String index(Model model, HttpServletRequest request)
	 {
		 String retorno = "403";
			
			HttpSession session = request.getSession();
			ObtenerPermisosPorUrl facilitador = new ObtenerPermisosPorUrl();
			session = facilitador.Obtener("/sgp/graficos", request, manage_entity,IDENTIFICADOR);
			permisos = (Permisos) session.getAttribute("permisos-de-"+IDENTIFICADOR);
			String usuario = ""+SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			System.out.println(usuario);
			
			if(permisos.isC()){
				
				List<String> consulta1 = new ArrayList<String>();
				String sql1 = "select solicitud.titulo, proyecto.duracion\r\n" + 
						"from solicitud\r\n" + 
						"inner join proyecto on solicitud.codsoliciutud=proyecto.codsolicitud";
				consulta1 = (List<String>) manage_entity.executeNativeQuery(sql1);	
				Gson gson= new Gson();
				String json=gson.toJson(consulta1);
				model.addAttribute("duracion", json);
				
				
				
				retorno = path+"chart";
			}
			return retorno;
	}
}
