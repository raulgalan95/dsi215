package com.sisbam.sgp.controller.administration;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.sisbam.sgp.controller.variety.ObtenerPermisosPorUrl;
import com.sisbam.sgp.dao.DaoImp;
import com.sisbam.sgp.entity.administration.Actividad;
import com.sisbam.sgp.entity.administration.Proyecto;
import com.sisbam.sgp.entity.administration.Subcategoria;
import com.sisbam.sgp.entity.administration.TipoProyecto;
import com.sisbam.sgp.entity.security.Permisos;

@Controller
public class SubcategoriaController {
	@Autowired
	private DaoImp manage_entity;
	
	private String path = "Administration/Subcategoria/";
	private static final String IDENTIFICADOR = "subC";
	
	private Permisos permisos;

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/subcategorias", method = RequestMethod.GET)
	
	//INDEX
	public String index(Model model, HttpServletRequest request) {
		
		String retorno = "403";
		
		HttpSession session = request.getSession();
		ObtenerPermisosPorUrl facilitador = new ObtenerPermisosPorUrl();
		session = facilitador.Obtener("/sgp/subcategorias", request, manage_entity,IDENTIFICADOR);
		permisos = (Permisos) session.getAttribute("permisos-de-"+IDENTIFICADOR);
			
		
		
		if(permisos.isR())
		{
			Subcategoria subcategoria = new Subcategoria();
			model.addAttribute("subcategoriaForm", subcategoria);
			model.addAttribute("subcategoria", null);

			List<Subcategoria> subcategorias = (List<Subcategoria>) this.manage_entity.getAll("Subcategoria");
			List<TipoProyecto> tipoProyectos = (List<TipoProyecto>) this.manage_entity.getAll("TipoProyecto");
	
	
				model.addAttribute("subcategorias", subcategorias);
				model.addAttribute("tipoProyectos",tipoProyectos);
			retorno = path+"subcategoria";
		}
		return retorno;
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/subcategorias/add", method = RequestMethod.GET)
	public String addActividad(Model model, HttpServletRequest request)  {
		
		
		String retorno = "403";
		if(permisos.isC()){
			Subcategoria subcategoria = new Subcategoria();
			List<TipoProyecto> tipoPoryectos = (List<TipoProyecto>) this.manage_entity.getAll("TipoProyecto");
			
			model.addAttribute("subcategoriaForm", subcategoria);
			model.addAttribute("subcategoria", null);
			model.addAttribute("tipoPoryectos", tipoPoryectos);
			
			retorno = path+"subcategoria-form";
		}
		return retorno;
		
	}
	
	
	//GUARDAR
		@RequestMapping(value = "/subcategorias/add", method = {RequestMethod.POST, RequestMethod.GET})
		public String saveOrUpadateSubcategoria(@ModelAttribute("subcategoriaForm") Subcategoria subcategoriaRecibida,Model model) throws ClassNotFoundException {
			String retorno = "403";
			if(permisos.isC())
			{
					Subcategoria subcategoria = subcategoriaRecibida;
					TipoProyecto tipoProyectoRecibido = (TipoProyecto) this.manage_entity.getById(TipoProyecto.class.getName(), subcategoria.getIdTipoProyecto());
					subcategoria.setTipoProyecto(tipoProyectoRecibido);
					if(subcategoria.getIdSubcategoria()==0) {
						manage_entity.save(Subcategoria.class.getName(), subcategoria);
					}else{
						manage_entity.update(Subcategoria.class.getName(), subcategoria);
					}
					retorno="redirect:/subcategorias";
			}
			return retorno;
		}
		
		//ACTUALIZAR
		@RequestMapping(value = "/subcategorias/update/{id}", method = RequestMethod.GET)
		public String update(@PathVariable("id") String idSubcategoria, Model model, HttpServletRequest request) throws ClassNotFoundException {
			String retorno="403";
			if(permisos.isU()) 
			{
				Subcategoria subcategoria = (Subcategoria) manage_entity.getById(Subcategoria.class.getName(), Integer.parseInt(idSubcategoria));
				model.addAttribute("subcategoria", subcategoria);
				Subcategoria subcategoriaForm = new Subcategoria();
				model.addAttribute("subcategoriaForm", subcategoriaForm);
				
				List<TipoProyecto> tipoProyectos = (List<TipoProyecto>) this.manage_entity.getAll("TipoProyecto");
				model.addAttribute("tipoProyectos", tipoProyectos);
				retorno=path+"subcategoria-form";
			}
			
			return retorno;
		}
		
		//ELIMINAR
		@RequestMapping(value = "/subcategorias/delete/{id}", method = RequestMethod.GET)
		public String delete(@PathVariable("id") String idSubcategoria, Model model) throws ClassNotFoundException {
			String retorno="403";
			if(permisos.isD()) {
			Subcategoria subcategoria = (Subcategoria) manage_entity.getById(Subcategoria.class.getName(), Integer.parseInt(idSubcategoria));
			manage_entity.delete(Subcategoria.class.getName(), subcategoria);
			model.addAttribute("subcategoria", subcategoria);
			
			Subcategoria subcategoriaForm = new Subcategoria();
			model.addAttribute("subcategoriaForm", subcategoriaForm);
			retorno="redirect:/subcategorias";
			}
			return retorno;
		}

}
