package com.sisbam.sgp.controller.security;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.ws.http.HTTPException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.sisbam.sgp.controller.variety.ObtenerPermisosPorUrl;
import com.sisbam.sgp.dao.DaoImp;
import com.sisbam.sgp.entity.security.Menu;
import com.sisbam.sgp.entity.security.Permisos;
import com.sisbam.sgp.entity.security.Vista;

import freemarker.core.ParseException;

import com.sisbam.sgp.controller.variety.ReportesController2;

@Controller
public class VistaController {
	
	@Autowired
	private DaoImp manage_entity;
	
	private String path="Security/Vista/";
	private static final String IDENTIFICADOR = "vistasx23";
	
	private Permisos permisos;
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/vistas", method = RequestMethod.GET)
	public String index(Model model, HttpServletRequest request) {
		
		String retorno = "403";
		
		HttpSession session = request.getSession();
		ObtenerPermisosPorUrl facilitador = new ObtenerPermisosPorUrl();
		session = facilitador.Obtener("/sgp/vistas", request, manage_entity,IDENTIFICADOR);
		permisos = (Permisos) session.getAttribute("permisos-de-"+IDENTIFICADOR);
			
	
		
		if(permisos.isR()) {
			Vista vista = new Vista();
			model.addAttribute("vistaForm", vista);
			model.addAttribute("vista", null);
			List<Vista> vistas= (List<Vista>) this.manage_entity.getAll("Vista");
			List<Menu> menus = (List<Menu>) this.manage_entity.getAll("Menu");
			model.addAttribute("vistas", vistas);
			model.addAttribute("menus", menus);
			retorno = path+"vista";
		}
		return retorno;
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/vistas/add", method = RequestMethod.GET)
	public String addVista(Model model, HttpServletRequest request)  {
		
		
		
		
		String retorno = "403";
		if(permisos.isC())
		{
			Vista vista = new Vista();
			List<Menu> menus = (List<Menu>) this.manage_entity.getAll("Menu");
			model.addAttribute("vistaForm", vista);
			model.addAttribute("vista", null);
			model.addAttribute("menus", menus);
			retorno = path+"vista-form";
		}
		return retorno;
		
	}
	
	@RequestMapping(value = "/vistas/add", method = RequestMethod.POST)
	public String saveOrUpadateVista(@ModelAttribute("vistaForm") Vista vistaRecibido) throws ClassNotFoundException {
		Vista vista = vistaRecibido;
		Menu menuSeleccionado = (Menu) this.manage_entity.getById(Menu.class.getName(), vista.getIdMenu());
		vista.setMenu(menuSeleccionado);
		if (permisos.isC()) {

			if (vista.getIdVista() == 0) {
				manage_entity.save(Vista.class.getName(), vista);
			} else {
				manage_entity.update(Vista.class.getName(), vista);
			}
			return "redirect:/vistas";
		}
		return "403";
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/vistas/update/{id}", method = RequestMethod.GET)
	public String update(@PathVariable("id") String vistaId, Model model, HttpServletRequest request) throws ClassNotFoundException {
		
		if(permisos.isU()) 
		{
				
		Vista vista = (Vista) manage_entity.getById(Vista.class.getName(), Integer.parseInt(vistaId));
		model.addAttribute("vista", vista);
		
		Vista vistaForm = new Vista();
		model.addAttribute("vistaForm", vistaForm);
		
		List<Menu> menus = (List<Menu>) this.manage_entity.getAll("Menu");
		model.addAttribute("menus", menus);
		return path+"vista-form";
		}
		return "403";
	}
	
	
	@RequestMapping(value = "/vistas/delete/{id}", method = RequestMethod.GET)
	public String delete(@PathVariable("id") String vistaId, Model model) throws ClassNotFoundException {
		if(permisos.isD()) {
		Vista vista = (Vista) manage_entity.getById(Vista.class.getName(), Integer.parseInt(vistaId));
		manage_entity.delete(Vista.class.getName(), vista);
		model.addAttribute("vista", vista);
		
		Vista vistaForm = new Vista();
		model.addAttribute("vistaForm", vistaForm);
		
		@SuppressWarnings("unchecked")
		List<Vista> vistas = (List<Vista>) this.manage_entity.getAll("Vista");
		model.addAttribute("vistas", vistas);
		return "redirect:/vistas";
		}
		return "403";
	}
	
	@RequestMapping("/quepex")
    public String generator() throws HTTPException {
        throw new HTTPException(404);
    }
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value = "/vistasPDF", method = RequestMethod.GET)
	
	public void indexPRUEBA(Model model, HttpServletRequest request,HttpServletResponse response) {
		ReportesController2 RP2 =  new ReportesController2();
		
		HashMap map = new HashMap<>();
		
		List lista_campos_header = new ArrayList<>();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		lista_campos_header.add("FECHA CREACION: "+sdf.format(new Date()));
		lista_campos_header.add("USUARIO: "+"Gabriel Fernando Pérez Sagastume");
		lista_campos_header.add("CODIGO REPORTE: "+manage_entity.getCodigoReporte());
		
		map.put("SQL", "select s.titulo as \"Projecto-20\", x.objgeneral as \"OBJETIVO-20\",  e.nombre || ' ' || e.apellidos as \"Encargado-12\" from proyecto  as x inner join solicitud as s on s.codsoliciutud= x.codsolicitud inner join usuario as u on u.id_usuario = s.idusuario inner join empleado as e on e.id_empleado = u.id_empleado ");
		map.put("CAMPOS",lista_campos_header);
		map.put("TITULO_REPORTE", "FACULTAD DE QUIMICA Y FARMACIA");
		map.put("NOMBRE_EMPRESA", "UNIVERSIDAD DE EL SALVADOR");
		map.put("LOGO","/apache-tomcat-9.0.10/Logoquimica.png");
		map.put("DESCRIPCION","Lista de proyectos en ejecucion en la facultad de quimica y farmacia");
		map.put("ORIENTACION_TEXTO", "Center");
		
		try {
			RP2.genearReporte(map, response);
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
}
