package com.sisbam.sgp.controller.variety;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import com.sisbam.sgp.configuration.HibernateConf;

public class JRXML_CONSTRUCTOR {

	public    String BACKGROUND = "\n\n<background> <band/> </background>\n";
	public    String XML_END_TAG = "\n</jasperReport>";
	public String ORIENTACION_TEXTO = "Center";
	
	public   String getHeader() {
		 String HEADER = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"+"\n"+
				 		 "<jasperReport xmlns=\"http://jasperreports.sourceforge.net/jasperreports\" "+ "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" "+ "xsi:schemaLocation=\"http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd\" "+"\n"+
					      "\t name=\""+("REPORTE_"+""+(new Date())).replaceAll(" ", "_").replaceAll(":", "_")+"\" "+ "\n"+
					      "\t pageWidth=\"595\" "+ "\n"+
					      "\t pageHeight=\"842\" "+ "\n"+
					      "\t columnWidth=\"535\""+"\n"+ 
					      "\t leftMargin=\"20\""+"\n"+
					      "\t rightMargin=\"20\""+ "\n"+
					      "\t topMargin=\"20\""+ "\n"+
					      "\t bottomMargin=\"20\""+"\n"+
					      "\t uuid=\"222d441b-0cf5-4442-b39d-bad3d2d50180\">\n"
					      +"<property name=\"com.jaspersoft.studio.data.sql.tables\" value=\"\"/> " + "\n"+
					      "<property name=\"com.jaspersoft.studio.data.defaultdataadapter\" value=\"DataAdapter.xml\"/>";
		return HEADER;
	}
	
	
	/*
	 * Recordar enviar la consulta SQL con los nombres como quiere que se muestren en el reporte
	 * Los nombres de las columnas deben llevar un guion seguido del tamaño necesario para darle de ancho asi: "SELECT b.nombreColumna as \"NOMBRE COLUMNA-10\" "
	 */
	public   String getSQLandFields(String sql) {
		String aux = limpiarSQL(sql);
		String SQL = "\n"+"<queryString language=\"SQL\">"+aux+"</queryString>"+"\n";
		
		List lista 				= getNombresColumnaSQL(aux);
		
		for (int i = 0; i < lista.size(); i++) {
			SQL =SQL +"\n"
				   + "	<field name=\""+lista.get(i)+"\" class=\"java.lang.String\">\r\n" + 
					 "		<property name=\"com.jaspersoft.studio.field.label\" value=\""+lista.get(i)+"\"/>\r\n" + 
					 "	</field>\n";
		}
		
		
		
		
		return SQL;
	}
	
	
	public   String getDetalle(String sqlLimpio,String sqSucio) {
		List lista = getNombresColumnaSQL(sqlLimpio);
		String detalle="\n"
			  + "	<detail>\r\n" + 
				"		<band height=\"20\">";
		
		List listaFormateada 	= formatearTamanios(getColumnasSQLconTamanios(sqSucio));
		int acumulador=0;
		
		
		
		
		for (int i = 0; i < listaFormateada.size(); i++) {
			
			detalle =detalle +"\n"
				 + "			<textField isStretchWithOverflow=\"true\">\r\n" + 
				   "					<reportElement stretchType=\"ElementGroupHeight\" x=\""+acumulador+"\" y=\"0\" width=\""+listaFormateada.get(i)+"\" height=\"20\" uuid=\"be689a59-a4b5-4b0f-a473-f9c36f6affea\">\r\n" + 
				   "						<property name=\"com.jaspersoft.studio.spreadsheet.connectionID\" value=\"1e609c43-29bf-48e2-9360-49710dcadf63\"/>\r\n" + 
				   "					</reportElement>\r\n" + 
				   "					<box padding=\"0\">\r\n" + 
				   "						<topPen    lineWidth=\"1.0\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\r\n" + 
				   "						<leftPen   lineWidth=\"1.0\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\r\n" + 
				   "						<bottomPen lineWidth=\"1.0\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\r\n" + 
				   "						<rightPen  lineWidth=\"1.0\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\r\n" + 
				   "					</box>\r\n" + 
				   "					<textElement textAlignment=\""+ORIENTACION_TEXTO+"\">\r\n" + 
				   "						<font size=\"14\"/>\r\n" + 
				   "					</textElement>\r\n" + 
				   "					<textFieldExpression><![CDATA[\" \"+$F{"+lista.get(i)+"}]]></textFieldExpression>\r\n" + 
				   "			</textField>";
			acumulador = acumulador + (int) listaFormateada.get(i);
		}
		
		detalle=detalle+"\n"
			  + "		</band>\r\n" + 
				"	</detail>";
		
		return detalle;
	}
	
	
	
	public   String getHeader(String name) {
		 String HEADER = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"+"\n"+
		 		 "<jasperReport xmlns=\"http://jasperreports.sourceforge.net/jasperreports\" "+ "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" "+ "xsi:schemaLocation=\"http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd\" "+"\n"+
			      "\t name=\""+name+"\" "+ "\n"+
			      "\t pageWidth=\"595\" "+ "\n"+
			      "\t pageHeight=\"842\" "+ "\n"+
			      "\t columnWidth=\"535\""+"\n"+ 
			      "\t leftMargin=\"20\""+"\n"+
			      "\t rightMargin=\"20\""+ "\n"+
			      "\t topMargin=\"20\""+ "\n"+
			      "\t bottomMargin=\"20\""+"\n"+
			      "\t uuid=\"222d441b-0cf5-4442-b39d-bad3d2d50180\">\n"
			      +"<property name=\"com.jaspersoft.studio.data.sql.tables\" value=\"\"/> " + "\n"+
			      "<property name=\"com.jaspersoft.studio.data.defaultdataadapter\" value=\"DataAdapter.xml\"/>";
		 return HEADER;
	}
	
	public   String getFooter() {
		String footer=""
				+"\n"
				+"<pageFooter>\r\n" + 
				"		<band height=\"17\">\r\n" + 
				"			<textField>\r\n" + 
				"				<reportElement mode=\"Opaque\" x=\"0\" y=\"4\" width=\"515\" height=\"13\" backcolor=\"#E6E6E6\" uuid=\"3ab8b66a-6a4c-46db-ae11-0a2f7e0d15a1\"/>\r\n" + 
				"				<textElement textAlignment=\"Right\"/>\r\n" + 
				"				<textFieldExpression><![CDATA[\"Pagina N. \"+$V{PAGE_NUMBER}+\" de\"]]></textFieldExpression>\r\n" + 
				"			</textField>\r\n" + 
				"			<textField evaluationTime=\"Report\">\r\n" + 
				"				<reportElement mode=\"Opaque\" x=\"515\" y=\"4\" width=\"40\" height=\"13\" backcolor=\"#E6E6E6\" uuid=\"86b852ea-f8e4-4610-8801-7da6184693d8\"/>\r\n" + 
				"				<textFieldExpression><![CDATA[\" \" + $V{PAGE_NUMBER}]]></textFieldExpression>\r\n" + 
				"			</textField>\r\n" + 
				"			<textField pattern=\"EEEEE dd MMMMM yyyy\">\r\n" + 
				"				<reportElement x=\"0\" y=\"4\" width=\"100\" height=\"13\" uuid=\"73a17ec9-12b5-47fd-8aec-a2b56d831ffd\"/>\r\n" + 
				"				<textFieldExpression><![CDATA[new java.util.Date()]]></textFieldExpression>\n" + 
				"			</textField>\r\n" + 
				"		</band>\r\n" + 
				"</pageFooter>\r\n" + 
				"";
		return footer;
	}
	
	
	
	public   String getHeaderFields(List camposList) {
		String headerFields = "\n"
				+ "<title>\r\n" + 
				"		<band height=\"155\">\n";
		if(camposList.size()>4) {
			System.err.println("LOS CAMPOS DE CABECERA NO DEBEN SER MAYORES QUE 4, HAS MANDADO: "+camposList.size());
		}
		switch(camposList.size()) {
			case 1: camposList.add("");camposList.add("");camposList.add("");break;
			case 2: camposList.add("");camposList.add("");
			case 3: camposList.add("");
		}
		
		headerFields=headerFields+"\n"
				+ " <!--campo1--><textField>\r\n" + 
				"  					<reportElement x=\"340\" y=\"20\" width=\"210\" height=\"30\" uuid=\"6fb82d64-d350-468b-9f6f-2c9597e9cbf1\"/>\r\n" + 
				"					<box>\r\n" + 
				"						<topPen    lineWidth=\"0.0\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\r\n" + 
				"						<leftPen   lineWidth=\"0.0\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\r\n" + 
				"						<bottomPen lineWidth=\"0.0\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\r\n" + 
				"						<rightPen  lineWidth=\"0.0\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\r\n" + 
				"					</box>\r\n" + 
				"					<textElement textAlignment=\"Left\">\r\n" + 
				"						<font size=\"11\"/>\r\n" + 
				"					</textElement>\r\n" + 
				"					<textFieldExpression><![CDATA[\""+camposList.get(0)+"\"]]></textFieldExpression>\r\n" + 
				"				</textField>\r\n" + 
				"  <!--campo2--><textField>\r\n" + 
				"					<reportElement x=\"340\" y=\"50\" width=\"210\" height=\"30\" uuid=\"5da666a3-010e-4a8c-9f13-f5c194013b7c\"/>\r\n" + 
				"					<box>\r\n" + 
				"						<topPen    lineWidth=\"0.0\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\r\n" + 
				"						<leftPen   lineWidth=\"0.0\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\r\n" + 
				"						<bottomPen lineWidth=\"0.0\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\r\n" + 
				"						<rightPen  lineWidth=\"0.0\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\r\n" + 
				"					</box>\r\n" + 
				"					<textElement textAlignment=\"Left\">\r\n" + 
				"						<font size=\"11\"/>\r\n" + 
				"					</textElement>\r\n" + 
				"					<textFieldExpression><![CDATA[\""+camposList.get(1)+"\"]]></textFieldExpression>\r\n" + 
				"				</textField>\n"
				+ " <!--campo3--><textField>\r\n" + 
				"					<reportElement x=\"120\" y=\"50\" width=\"210\" height=\"30\" uuid=\"dffb3660-be7d-4d29-918d-af314066adce\"/>\r\n" + 
				"					<box>\r\n" + 
				"						<topPen    lineWidth=\"0.0\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\r\n" + 
				"						<leftPen   lineWidth=\"0.0\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\r\n" + 
				"						<bottomPen lineWidth=\"0.0\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\r\n" + 
				"						<rightPen  lineWidth=\"0.0\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\r\n" + 
				"					</box>\r\n" + 
				"					<textElement textAlignment=\"Left\">\r\n" + 
				"						<font size=\"11\"/>\r\n" + 
				"					</textElement>\r\n" + 
				"					<textFieldExpression><![CDATA[\""+camposList.get(2)+"\"]]></textFieldExpression>\r\n" + 
				"				</textField>\n"
				+ " <!--campo4--><textField evaluationTime=\"Report\">\r\n" + 
				"					<reportElement x=\"120\" y=\"20\" width=\"210\" height=\"30\" uuid=\"458e2bf7-37f9-4805-923d-8686965425b5\"/>\r\n" + 
				"					<box>\r\n" + 
				"						<topPen    lineWidth=\"0.0\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\r\n" + 
				"						<leftPen   lineWidth=\"0.0\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\r\n" + 
				"						<bottomPen lineWidth=\"0.0\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\r\n" + 
				"						<rightPen  lineWidth=\"0.0\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\r\n" + 
				"					</box>\r\n" + 
				"					<textElement textAlignment=\"Left\">\r\n" + 
				"						<font size=\"11\"/>\r\n" + 
				"					</textElement>\r\n" + 
				"					<textFieldExpression><![CDATA[\"Numero de paginas: \"+$V{PAGE_NUMBER}]]></textFieldExpression>\r\n" + 
				"				</textField>";
		
		return headerFields;
	}
	
	public   String getCompanyName(String company) {
		String comp="\n<!--TITULO COMANIA-->\n"
			  + "				<textField>\r\n" + 
				"					<reportElement x=\"0\" y=\"-10\" width=\"555\" height=\"20\" uuid=\"6bf736bc-b652-4bb3-b22c-e9621f5fc8c5\"/>\r\n" + 
				"					<textElement textAlignment=\"Center\">\r\n" + 
				"						<font size=\"14\" isBold=\"true\"/>\r\n" + 
				"					</textElement>\r\n" + 
				"					<textFieldExpression><![CDATA[\""+company+"\"]]></textFieldExpression>\n"+
				"				</textField>";
		return comp;
	}
	
	
	public   String getLogo(String logo) {
		String logoc = "\n<!--LOGO COMPANIA-->\n"
			  + "				<image>\r\n" + 
				"					<reportElement x=\"0\" y=\"10\" width=\"100\" height=\"99\" uuid=\"2aca5b7a-003c-4eb4-809f-9aca7ce53138\"/>\r\n" + 
				"					<imageExpression><![CDATA[\""+logo+"\"]]></imageExpression>\r\n" + 
				"				</image>";
		return logoc;	
	}
	
	public   String getReportTitle(String titulo) {
		String title = "\n<!--TITULO REPORTE-->\n"
			+   "				<textField>\r\n" + 
				"					<reportElement mode=\"Opaque\" x=\"120\" y=\"80\" width=\"435\" height=\"42\" forecolor=\"#000000\" backcolor=\"#FFFFFF\" uuid=\"ef6d4bd9-43e5-4669-85ef-d2602e7ff56f\">\r\n" + 
				"						<property name=\"com.jaspersoft.studio.spreadsheet.connectionID\" value=\"1e609c43-29bf-48e2-9360-49710dcadf63\"/>\r\n" + 
				"					</reportElement>\r\n" + 
				"					<textElement textAlignment=\"Left\">\r\n" + 
				"						<font size=\"16\" isBold=\"true\"/>\r\n" + 
				"					</textElement>\r\n" + 
				"					<textFieldExpression><![CDATA[\""+titulo+"\"]]></textFieldExpression>\r\n" + 
				"				</textField>";
		return title;
	}
	
	public   String getReportDescription(String descripcion) {
		String description = "\n<!--DESCRIPCION,PARAMETROS,ETC-->\n"
			  + "				<textField>\r\n" + 
				"					<reportElement x=\"0\" y=\"123\" width=\"554\" height=\"30\" uuid=\"9a3c37f8-060f-44d6-bef0-d4ae4556df6b\"/>\r\n" + 
				"					<box>\r\n" + 
				"						<topPen    lineWidth=\"0.0\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\r\n" + 
				"						<leftPen   lineWidth=\"0.0\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\r\n" + 
				"						<bottomPen lineWidth=\"0.0\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\r\n" + 
				"						<rightPen  lineWidth=\"0.0\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\r\n" + 
				"					</box>\r\n" + 
				"					<textElement textAlignment=\""+ORIENTACION_TEXTO+"\">\r\n" + 
				"						<font size=\"12\"/>\r\n" + 
				"					</textElement>\r\n" + 
				"					<textFieldExpression><![CDATA[\""+descripcion+"\"]]></textFieldExpression>\r\n" + 
				"				</textField>"
				+ "\n		</band>\n" + 
				"</title>";
		
		return description;
		
	}
	
	/*
	 * el string contenido en la lista debe ir asi: "nombre(string)-tamañocolumna(int)"
	 */
	public  String getColumnReportHeader(List listaColumnasReporteCalc,boolean ordenar) {
		List listaColumnasReporte = new ArrayList<>();
		if(ordenar) {
			
			listaColumnasReporte = ordenarListaAsc(listaColumnasReporteCalc);
		}
		else {
		    listaColumnasReporte=listaColumnasReporteCalc;
		}
		
		
		double 	RAZON_PX_CARAC = 111/13;
		int 	ANCHO_TOTAL    = 555;
		List    ListaFormateada = new ArrayList<>();
		
		int	 	posicionX = 0;
		String 	columnas =  "\n<pageHeader> <band height=\"7\"/> </pageHeader>\r\n" + 
							"		<columnHeader>\r\n" + 
							"			<band height=\"25\">";
		
		for(int i =0;i<listaColumnasReporte.size();i++) {
			
			try {
				String valorColumna     = (String) listaColumnasReporte.get(i);
				String tamanioCol 		= valorColumna.split("-")[1];
				String nombreCol  		= valorColumna.split("-")[0];
				int tamanioColEntero 	= Integer.parseInt(""+tamanioCol);
				
							
				
				Double anchoCol = RAZON_PX_CARAC*tamanioColEntero;
				tamanioColEntero = (int) anchoCol.doubleValue();
				
				
				//SI VIENE UNA COLUMNA MUY GRANDE QUE OCUPA TODA LA PAGINA
				if(tamanioColEntero>ANCHO_TOTAL) {
					tamanioColEntero=555;
					System.out.println("IF 1");
		    		i=1;
					ListaFormateada.add(nombreCol+"-"+tamanioColEntero);
		    		int espacioSobrante = ANCHO_TOTAL-(posicionX+tamanioColEntero);
					columnas=columnas+rellenarColumnas(ListaFormateada,espacioSobrante);
					i=999999;
					columnas = columnas +"			</band>\r\n" + 
							 "		</columnHeader>";		
					return columnas;
				}
				
				
				
			    if((tamanioColEntero+posicionX)>ANCHO_TOTAL||(i+1)>=listaColumnasReporte.size()) {
			    	
			    	
			    	//si es de una sola columna enorme
			    	if(tamanioColEntero+posicionX>=ANCHO_TOTAL) {
			    		i=1;
						ListaFormateada.add(nombreCol+"-"+tamanioColEntero);
			    		int espacioSobrante = ANCHO_TOTAL-(posicionX+tamanioColEntero);
						columnas=columnas+rellenarColumnas(ListaFormateada,espacioSobrante);
						i=999999;
			    	}
			    	
			    	//si se acabo de iterar y no es necesario quitar columnas
			    	if((i+1)>=listaColumnasReporte.size()) {
						ListaFormateada.add(nombreCol+"-"+tamanioColEntero);
			    		int espacioSobrante = ANCHO_TOTAL-(posicionX+tamanioColEntero);
						columnas=columnas+rellenarColumnas(ListaFormateada,espacioSobrante);
						i=999999;
			    	}
			    	//si se termino de iterar y ya no hay espacio para la ultima columna
			    	else {
			    		int espacioSobrante = ANCHO_TOTAL-posicionX;
						columnas=columnas+rellenarColumnas(ListaFormateada,espacioSobrante);
						i=999999;
			    	}
			    	
					
				}
				else {
					ListaFormateada.add(nombreCol+"-"+tamanioColEntero);
					posicionX = posicionX+tamanioColEntero;
				}
			}
			catch(Exception e) {
				System.out.println("FORMATO DE LA LISTA DE COLUMNAS FIJAS DEL REPORTE ESTA INCORRECTO: JRXML_ S.get ColumnReportHeader()"+e);
			}
			
		}
		
		columnas = columnas +"			</band>\r\n" + 
							 "		</columnHeader>";		
		return columnas;
	}
	
	private   String rellenarColumnas(List columnasFormateadas,int espacioSobrante) {
		
		String columnas="";
		int	 	posicionX = 0;
		int     cantidadEquitativaParaRepartir = (int) espacioSobrante/columnasFormateadas.size();
		
		for(int i =0;i<columnasFormateadas.size();i++) {
			
			try {
				String valorColumna     = (String) columnasFormateadas.get(i);
				String tamanioCol 		= valorColumna.split("-")[1];
				String nombreCol  		= valorColumna.split("-")[0];
				int tamanioColEntero 	= Integer.parseInt(""+tamanioCol);
				
				tamanioColEntero = tamanioColEntero+cantidadEquitativaParaRepartir;
				
				
					columnas = columnas+
							"\n"
						  + "			<staticText>\r\n" + 
							"					<reportElement mode=\"Opaque\" x=\""+posicionX+"\" y=\"0\" width=\""+tamanioColEntero+"\" height=\"25\" forecolor=\"#FFFFFF\" backcolor=\"#000000\" uuid=\"46b45c8f-0345-4066-9746-c88ab8e66955\">\r\n" + 
							"						<property name=\"com.jaspersoft.studio.spreadsheet.connectionID\" value=\"1e609c43-29bf-48e2-9360-49710dcadf63\"/>\r\n" + 
							"					</reportElement>\r\n" + 
							"					<box>\r\n" + 
							"						<pen lineWidth=\"1.0\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\r\n" + 
							"					</box>\r\n" + 
							"					<textElement textAlignment=\""+ORIENTACION_TEXTO+"\">\r\n" + 
							"						<font size=\"12.5\" isBold=\"true\"/>\r\n" + 
							"					</textElement>\r\n" + 
							"					<text><![CDATA[ "+nombreCol+"]]></text>\r\n" + 
							"			</staticText>\n";
					
					
					posicionX = posicionX+tamanioColEntero;
				
			}
			catch(Exception e) {
				System.err.println("FORMATO DE LA LISTA DE COLUMNAS FIJAS DEL REPORTE ESTA INCORRECTO: JRXML_ S.get ColumnReportHeader()"+e);
			}
			
		}
		
		return columnas;
		
	}
	
	private   List ordenarListaAsc(List list) {
		List listOrder = new ArrayList<>();
		int size = list.size();
		for(int i =0;i<size;i++) {
			int iMayor = IndiceMayor(list);
			listOrder.add(list.get(iMayor));
			list.remove(iMayor);
		}
		return listOrder;
	}
	
	private   int IndiceMayor(List list) {
		int ValorMayor =0;
		int indiceMayor = 0;
		for(int i=1;i<list.size();i++) {
			String valorColumnaActual   = (String) list.get(i);
			String tamanioColActual 	= valorColumnaActual.split("-")[1];
			String nombreActual         = valorColumnaActual.split("-")[1];
			int actual 					= Integer.parseInt(""+tamanioColActual);
			
			if(actual>ValorMayor) {
				ValorMayor = actual;
				indiceMayor = i;
			}
		}
		return indiceMayor;
	}
	
	/*
	 * METODO PARA OBTENER LOS NOMBRES DE LA COLUMNA CON SU RESPECTIVO TAMAÑO APARTIR DE UNA CONSULTA SQL
	 */
	public   List getNombresColumnaSQL(String sql) {
		List columnasSQL = new ArrayList<>();
		try {
			HibernateConf hb = new HibernateConf();
			DataSource ds = hb.restDataSource();
			Statement consulta;
			ResultSet data;
			
			
			Connection conection = ds.getConnection();
			consulta = conection.createStatement();
			data = consulta.executeQuery(sql);
			ResultSetMetaData rsmd = data.getMetaData();
			int columnCount = rsmd.getColumnCount();
			
			for (int i = 1; i <= columnCount; i++ ) {
				  String name = rsmd.getColumnName(i);
				  columnasSQL.add(name);
				  
				}
			
		}
		
		catch (Exception e) {
			System.err.println("ERROR OBTENIENDO NOMBRES DE COLUMNA DE LA CONSULTA SQL "+e);
		}
		return columnasSQL;
		
	}
	
	/*
	 * METODO PARA OBTENER EL NOMBRE DE LAS COLUMNAS SQL PARA LLENAR LA LISTA DE DETALLE
	 */
	public   List getNombresColumnaFields(String sql) {
		List columnasSQL = new ArrayList<>();
		try {
			HibernateConf hb = new HibernateConf();
			DataSource ds = hb.restDataSource();
			Statement consulta;
			ResultSet data;
			
			
			Connection conection = ds.getConnection();
			consulta = conection.createStatement();
			data = consulta.executeQuery(sql);
			ResultSetMetaData rsmd = data.getMetaData();
			int columnCount = rsmd.getColumnCount();
			
			for (int i = 1; i <= columnCount; i++ ) {
				  String name = rsmd.getColumnName(i);
				  columnasSQL.add(name);
				  
				}
			
		}
		
		catch (Exception e) {
			System.err.println("ERROR OBTENIENDO NOMBRES DE COLUMNA DE LA CONSULTA SQL "+e);
		}
		return columnasSQL;
		
	}
	
	/*
	 * Recibe una consulta SQL con el formato COLUMNA-TAMAÑO y la devulve normal sin ese formato
	 */
	public   String limpiarSQL(String sql) {
		String[] sqLimpio1;
		sql = sql.toUpperCase();
		sqLimpio1 = sql.split(",");
		String[] demasSQL = sql.split(" FROM ");
		String sqlNew="";
		for(int i =0;i<sqLimpio1.length;i++) {
			String[] aux = sqLimpio1[i].split(" AS ");			
				String aux2 = sqlNew+sqLimpio1[i];
				if((i+1)>=sqLimpio1.length) {
					sqlNew = aux2.split(" AS ")[0]+"";
				}
				else {
					sqlNew = aux2.split(" AS ")[0]+",";
				}
		}
		sqlNew=sqlNew+" FROM "+demasSQL[1];
		return sqlNew;
	}
	
	

	public   List getColumnasSQLconTamanios(String sql) {
		List tamaniosList = new ArrayList<>();
		String[] sqLimpio1;
		sql = sql.toUpperCase();
		String[] AUX = sql.split(" FROM ");
		String CONS = AUX[0];
		sqLimpio1 = CONS.split(",");
		String[] tamanios = {""};
		String sqlNew="";
		for(int i =0;i<sqLimpio1.length;i++) {
			String 		AUX1 	= sqLimpio1[i];
			String[] 	SplitAS = AUX1.split(" AS ");
			String  	AUX2 	= SplitAS[1].replaceAll("\"", "");
			String[] 	SplitG	= AUX2.split("-");
			String		AUX3 	= SplitG[1].replaceAll("\"", "");;
			tamaniosList.add(AUX3);
		}
		return tamaniosList;
	}
	
	public   List formatearTamanios(List listaTamanios) {
		List tamaniosFormateados= new ArrayList<>();
		double 	RAZON_PX_CARAC = 111/13;
		int 	ANCHO_TOTAL    = 555;
		int posicionX=0;
		String columnas="";
		int espacioSobrante=555;
		for (int i = 0; i < listaTamanios.size(); i++) {
			int tamanioEntero = Integer.parseInt(""+listaTamanios.get(i));
			Double anchoCol = RAZON_PX_CARAC*tamanioEntero;
			int tamanioColEntero = (int) anchoCol.doubleValue();
			
			if(tamanioColEntero>ANCHO_TOTAL) {
				tamanioColEntero=ANCHO_TOTAL;
				i=1;
				tamaniosFormateados.add(tamanioColEntero);
				return tamaniosFormateados;
			}
			
			if((tamanioColEntero+posicionX)>ANCHO_TOTAL||(i+1)>=listaTamanios.size()) {
//				si es de una sola columna enorme
				if(tamanioColEntero+posicionX>=ANCHO_TOTAL) {
					i=1;
					tamaniosFormateados.add(tamanioColEntero);
					espacioSobrante=espacioSobrante-tamanioColEntero;
					return repartirSobrante(tamaniosFormateados,espacioSobrante);
				}
				
				if((i+1)>=listaTamanios.size()) {
					i=1;
					tamaniosFormateados.add(tamanioColEntero);
					espacioSobrante=espacioSobrante-tamanioColEntero;
					return repartirSobrante(tamaniosFormateados,espacioSobrante);
				}
				
			}
			else {
				tamaniosFormateados.add(tamanioColEntero);
				espacioSobrante=espacioSobrante-tamanioColEntero;
			}
			
			posicionX=posicionX+tamanioColEntero;
		}
		return repartirSobrante(tamaniosFormateados,espacioSobrante);
	}
	
	public   List repartirSobrante(List lista,int sobrante){
		int     cantidadEquitativaParaRepartir = (int) sobrante/lista.size();
		List listaFormateados = new ArrayList<>();
		int	 	posicionX = 0;
		for (int i = 0; i < lista.size(); i++) {
			int tamanioColumna =(int) lista.get(i);
			tamanioColumna=tamanioColumna+cantidadEquitativaParaRepartir;
			listaFormateados.add(tamanioColumna);
		}
		return listaFormateados;
	}
	
	
}
