package com.sisbam.sgp.controller.variety;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.HashMap;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.stereotype.Controller;
import com.sisbam.sgp.configuration.HibernateConf;
import freemarker.core.ParseException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperRunManager;

@Controller
public class ReportesController2 {
/**
 * CLASE PARA GENERAR REPORTES
 * 
 * 
 * *<hr>
	 *EJEMPLO:<hr>
	 *<br>HashMap map = new HashMap<>();
			<br>
			<br>List lista_campos_header = new ArrayList<>();
			<br>SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			<br>lista_campos_header.add("FECHA CREACION: "+sdf.format(new Date()));
			<br>lista_campos_header.add("USUARIO: "+"BRYAN ARTHUR");
			<br>lista_campos_header.add("CODIGO REPORTE: "+"0000000");
			<br>lista_campos_header.add("NUMERO DE PAGINAS: "+"45");
			<br>
			<br>map.put("SQL", "select  c.codigo as \"CODIGO-10\",c.nombre as \"CUENTA-55\" from cuentacontable c order by c.codigo");
			<br>map.put("CAMPOS",lista_campos_header);
			<br>map.put("TITULO_REPORTE", "ESTE ES EL TITULO DEL REPORTE");
			<br>map.put("NOMBRE_EMPRESA", "EL PAJARITO S.A. DE C.V.");
			<br>map.put("LOGO","/logoc.png");
			<br>map.put("DESCRIPCION","DESDE:08/08/2018 \tHASTA:15/08/2019 \tLUGAR: SAN SALVADOR \tPARAMETRO 2: 12341234 \tPARAMETRO 3: XXXXXXXXXXX \tPARAMETRO 4:ñññññññ");
			<br>map.put("ORIENTACION_TEXTO", "Left");
			<br>
			
			<br>JRXML_CREATE cc = new JRXML_CREATE();
			<br>String RutaJRXML = cc.GetJRXMLPath(map);
 * 
 * @author B.Miranda
 * @param  response: para mostrar reporte
 * @param  hmParams: Hash map parameters
 */	
public void genearReporte(	HashMap mapa,			
							HttpServletResponse 	response
						 	) throws ParseException,Exception {		//-los throws son para que la clase ExceptionsController 
																	//se coma el error y no le muestre al usuario un error feo del tomcat
		
		//inicializa una conexion
		Connection conn = null;
		//obtiene los datos de conexion a la BD por medio de hibernate para no tener que ponerlos de nuevo
		HibernateConf datosDeConexionBD = new HibernateConf();
		//Basic datasorce es una clase de propiedades solo contiene nombre de la base driver usado, etc
		BasicDataSource datosConex = (BasicDataSource) datosDeConexionBD.restDataSource();
			
			//se obtiene el reporte como objeto atravez del archivo .jasper ya generado
			
			
			
			JRXML_CREATE JC = new JRXML_CREATE();
			 conn = DriverManager.getConnection(datosConex.getUrl(),datosConex.getUsername(),datosConex.getPassword());
			String path = JasperCompileManager.compileReportToFile( JC.GetJRXMLPath(mapa));
//			InputStream reportStream = request.getSession().getServletContext().getResourceAsStream(path);
			File ARCHIVO_COMPILADO = new File(path);
			InputStream reportStream = null;
			
			reportStream=new FileInputStream(ARCHIVO_COMPILADO);
			
			//se intenta obtener el flujo de vista
			try (ServletOutputStream servletOutputStream = response.getOutputStream()) {
				//conexion a la BD
			     Class.forName(datosConex.getDriverClassName());
			     conn = DriverManager.getConnection(datosConex.getUrl(),datosConex.getUsername(),datosConex.getPassword());
			     

			     response.setContentType("application/pdf");
			     JasperRunManager.runReportToPdfStream(reportStream, servletOutputStream, (new HashMap()), conn);
			     
			     servletOutputStream.flush();
		    	 servletOutputStream.close();
			     conn.close();
			     if(ARCHIVO_COMPILADO.delete()) 
			     {
			    	 System.err.println("Archivo eliminado!");
			     }
				
		        
			}
			catch(Exception e) {
				System.out.println("ERROR LINEA 04 REPORTESCONTROLERR2"+e);
				e.printStackTrace();
			}
		
	}

	
}
