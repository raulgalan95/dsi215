package com.sisbam.sgp.controller.variety;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


public class JRXML_CREATE {
	
	/**
	 *Este metodo Recibe un HashMap y devuelve la ruta de guardado del archivo .JRXML para compilar
	 *Los datos necesarios son estos:
	 *<br>(String)map.get("ORIENTACION_TEXTO"); // "Left" "Center"
	 *<br>(String)map.get("SQL") //en este formato: SELECT T.COLUMNA AS "COLUMNA-10" FROM TABLA T EN COLUMNA-10, COLUMNA ES EL NOMBRE A MOSTRAR Y 10 EL TAMAÑO MAX EN CARACTERES	
	 *<br>(List)map.get("CAMPOS") //una lista de los campos a mostrar en la cabecera son 4 campos maximo
	 *<br>(String)map.get("NOMBRE_EMPRESA")
	 *<br>(String)map.get("LOGO") //logo de la empresa ejemplo: "/LOGOC.PNG" 
	 *<br>(String)map.get("TITULO_REPORTE")
	 *<br>(String)map.get("DESCRIPCION")
	 *
	 *<br>
	 *@author bmiranda
	 *<hr>
	 *EJEMPLO:
	 *<br>HashMap map = new HashMap<>();
			<br>
			<br>List lista_campos_header = new ArrayList<>();
			<br>SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			<br>lista_campos_header.add("FECHA CREACION: "+sdf.format(new Date()));
			<br>lista_campos_header.add("USUARIO: "+"BRYAN ARTHUR");
			<br>lista_campos_header.add("CODIGO REPORTE: "+"0000000");
			<br>lista_campos_header.add("NUMERO DE PAGINAS: "+"45");
			<br>
			<br>map.put("SQL", "select  c.codigo as \"CODIGO-10\",c.nombre as \"CUENTA-55\" from cuentacontable c order by c.codigo");
			<br>map.put("CAMPOS",lista_campos_header);
			<br>map.put("TITULO_REPORTE", "ESTE ES EL TITULO DEL REPORTE");
			<br>map.put("NOMBRE_EMPRESA", "EL PAJARITO S.A. DE C.V.");
			<br>map.put("LOGO","/logoc.png");
			<br>map.put("DESCRIPCION","DESDE:08/08/2018 \tHASTA:15/08/2019 \tLUGAR: SAN SALVADOR \tPARAMETRO 2: 12341234 \tPARAMETRO 3: XXXXXXXXXXX \tPARAMETRO 4:ñññññññ");
			<br>map.put("ORIENTACION_TEXTO", "Left");
			<br>
			
			<br>JRXML_CREATE cc = new JRXML_CREATE();
			<br>String RutaJRXML = cc.GetJRXMLPath(map);
	 */
	public String GetJRXMLPath(HashMap map) {
		String RUTA_GUARDADO_ARCHIVO_JRXML="";
		String JRXML_CODE="";
			
			ValidarMap(map);
			JRXML_CONSTRUCTOR JC = new JRXML_CONSTRUCTOR();
			JC.ORIENTACION_TEXTO=""+map.get("ORIENTACION_TEXTO");
			JRXML_CODE  =   
						    JC.getHeader()         								
					       +JC.getSQLandFields(""+map.get("SQL"))									 					
					       +JC.BACKGROUND
					       +JC.getHeaderFields((List)map.get("CAMPOS"))    		
					       +JC.getCompanyName((String)map.get("NOMBRE_EMPRESA"))
					       +JC.getLogo(""+map.get("LOGO"))
					       +JC.getReportTitle(""+map.get("TITULO_REPORTE"))
					       +JC.getReportDescription(""+map.get("DESCRIPCION"))
					       +JC.getColumnReportHeader(JC.getNombresColumnaSQL(""+map.get("SQL")),false)
					       +JC.getDetalle(JC.limpiarSQL(""+map.get("SQL")),""+map.get("SQL"))
						   +JC.getFooter()				 						
						   +JC.XML_END_TAG;				 						
			RUTA_GUARDADO_ARCHIVO_JRXML = Log(JRXML_CODE);
			
		return RUTA_GUARDADO_ARCHIVO_JRXML;
	}
    
    public String  Log(String log){
    	 File archivo2 = null;
        try{
             archivo2 = new File("" + ("/apache-tomcat-9.0.10/REPORTE_"+""+(new Date())).replaceAll(" ", "_").replaceAll(":", "_")+".jrxml");
//              archivo2 = new File("" +"/home/arthur/REPORTE.jrxml");
              
              if(archivo2.createNewFile()){
                    System.out.println("Archivo creado!");
                    }
              System.err.println(""+archivo2.getAbsolutePath());      
                    
              FileWriter escritor = new FileWriter(archivo2,false);
              BufferedWriter ayudaEscritor= new BufferedWriter(escritor);
              ayudaEscritor.write(log); //datos es una variable de tipo String[].
              ayudaEscritor.newLine();
              ayudaEscritor.close();
        }
        catch(Exception e){
              System.out.println("ERROR ESCRIBIENDO LOG");
        }
        
        return ""+archivo2.getAbsolutePath();
  }
  
  public void ValidarMap(HashMap map) {
	  if(map.get("SQL") == null) {
			System.err.println("FALTA LA LLAVE \"SQL\" EN EL MAPA ENVIADO");
		}
		
		if(map.get("CAMPOS") == null) {
			System.err.println("FALTA LA LLAVE \"CAMPOS\" EN EL MAPA ENVIADO");
		}
		if(map.get("NOMBRE_EMPRESA")==null) {
			System.err.println("FALTA LA LLAVE \"NOMBRE_EMPRESA\" EN EL MAPA ENVIADO");
		}
		if(map.get("DESCRIPCION")==null) {
			System.err.println("FALTA LA LLAVE \"DESCRIPCION\" EN EL MAPA ENVIADO");
		}
		if(map.get("DESCRIPCION")==null) {
			System.err.println("FALTA LA LLAVE \"TITULO_REPORTE\" EN EL MAPA ENVIADO");
		}
		

  }
  
  
    

    
}
//------------------------------------------------------------------------------------------------------------------------------------