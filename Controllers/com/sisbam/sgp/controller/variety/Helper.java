package com.sisbam.sgp.controller.variety;

import java.util.HashMap;
import java.util.Iterator;

/**
 * Creado el 21/03/2018
 * 
 * @author Bryan Miranda
 * 
 */

public class Helper {
	/**
	 * Completa el tamanio de un String (A la izquierda o a la derecha) con un caracter que se le indique
	 * @param cad		Objeto String para completar
	 * @param caracter  Caracter de relleno
	 * @param len       Cantidad de caracteres a rellenar
	 * @param derecha	Si es verdadero rellena a la derecha, falso rellena a la izquierda 
	 * 
	 * Ejemplo: CompleteWith("hola", "0", 10,true), Retorna: "hola000000"
	 * 
	 */
    public static String CompleteStringWith(String cad,String caracter,int len,boolean derecha){
    	
    	//Si el String ya cumple con el tama�o lo devuelve igual.
    	if(cad.length()==len){
    		return cad;
    	}
    	else
    	{
	    		//Si el String es mayor que el tama�o, le corta una parte.
	    		if(cad.length()>len){
	    			return cad.substring(0,len);
	    		}
	    		
	    		//Si hay que rellenar y es a la derecha
	    		if(cad.length()<len&&derecha)
	    		{
	    			int tama = len-cad.length();
	    			for(int i=0;i<tama;i++)
	    			{
	    				cad=cad+caracter;
	    			}
	    		}
	    		//Si hay que rellenar y es a la izquierda
	    		if(cad.length()<len&&!derecha)
	    		{
	    			int tama = len-cad.length();
	    			for(int i=0;i<tama;i++)
	    			{
	    				cad=caracter+cad;
	    			}
	    		}
    	}
    	
    	return cad;
    }
    
    /**  
     * Recibe un json y lo coloca en un mapa java
     * (Si el json contiene otro Json dentro lo tomara como String, este no se convierte)
     * 
     * @param json
     * @return HashMap<String,Object>
     */
    public static HashMap<String, Object> JsonToMap(String json){
		HashMap<String,Object> mapa = new HashMap<String, Object>();
		if(json.endsWith("}")&&json.startsWith("{")) {
			json=json.replace("{", "").replace("}", "");
		}
		else {
			mapa.put("error", "no es un json");
			return mapa;
		}
		String[] keys;
		int i=0;
		keys=json.split(",");
		
		while(i<keys.length) {
			String aux = keys[i];
			mapa.put(aux.split(":")[0].replace("\"", ""), aux.split(":")[1].replace("\"", ""));
			i++;
		}
		return mapa;
	}
	
	
	 /**
     * Toma un mapa Hash de java y devuelve un String en formato json con los valores enviados
     * 
     * @param  map
     * @return (String)Json
     */
    public static String MapToJsonString(HashMap<String,Object> map){
    	
    	String json = "{";
    	try{
		    	Iterator it = map.keySet().iterator();
		    	Iterator it2 = map.values().iterator();
		    	
		    	while(it.hasNext()&&it2.hasNext()){
		    		String key = ""+it.next().toString().trim().replace(" ", "");
		    		json=json+"\""+key+"\""+":"+it2.next()+",";
		    	}
		    	if(json.endsWith(",")){
		    		json=json.substring(0, json.length()-1);
		    	}
		    	json=json+"}";
		    	return json;
    	}
    	catch(Exception e){
    		json="{\"error\":\"Error convirtiendo mapa- "+e+"\"}";
    		return json;
    	}
    }
}
