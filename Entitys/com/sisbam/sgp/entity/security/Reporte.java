package com.sisbam.sgp.entity.security;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "reporte")
public class Reporte {
	private int idReporte;

	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "reporte_id_reporte_seq")
	@SequenceGenerator(name = "reporte_id_reporte_seq", sequenceName = "reporte_id_reporte_seq", allocationSize = 1)
	public int getIdReporte() {
		return idReporte;
	}

	public void setIdReporte(int idReporte) {
		this.idReporte = idReporte;
	}
	
	
	
}
