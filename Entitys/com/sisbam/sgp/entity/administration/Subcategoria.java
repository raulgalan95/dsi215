package com.sisbam.sgp.entity.administration;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "subcategoria", catalog = "sgp")
public class Subcategoria implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private int idSubcategoria;
	private String nombre;
	private TipoProyecto tipoProyecto;
	
	public Subcategoria() {
	
	}

	public Subcategoria(int idSubcategoria, String nombre, TipoProyecto tipoProyecto) {
	
		this.idSubcategoria = idSubcategoria;
		this.nombre = nombre;
		this.tipoProyecto = tipoProyecto;
	}
	
	@Transient
	private int idTipoProyecto;

	@Transient
	public int getIdTipoProyecto() {
		return idTipoProyecto;
	}

	@Transient
	public void setIdTipoProyecto(int idTipoProyecto) {
		this.idTipoProyecto = idTipoProyecto;
	}
    
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "subcategoria_idsubcategoria_seq")
	@SequenceGenerator(name = "subcategoria_idsubcategoria_seq", sequenceName = "subcategoria_idsubcategoria_seq", allocationSize = 1)
	@Column(name = "idSubcategoria", unique = true, nullable = false)
	public int getIdSubcategoria() {
		return idSubcategoria;
	}

	public void setIdSubcategoria(int idSubcategoria) {
		this.idSubcategoria = idSubcategoria;
	}
	@Column(name = "nombre", nullable = false, length = 50)
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idTipoProyecto")
	public TipoProyecto getTipoProyecto() {
		return tipoProyecto;
	}

	public void setTipoProyecto(TipoProyecto tipoProyecto) {
		this.tipoProyecto = tipoProyecto;
	}
	
	
	
	
	
	
}
