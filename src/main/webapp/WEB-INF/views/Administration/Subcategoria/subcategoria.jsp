<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
             
<div id="contenido" class="card-panel hoverable" style="margin-bottom:15%;">
<c:if test="${createsubC}">	
		<a class="waves-effect waves-light btn-floating modal-trigger green" href="#agregar"><i class="fa fa-plus-circle" aria-hidden="true"></i>Agregar</a>&nbsp;&nbsp;
</c:if>	
		
		<hr>	
	 	<div class="container">	
<c:if test="${readsubC}">	
			<table id="tabla" class="display hover cell-border"  cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>Nombre</th>
						<th>Tipo de Proyecto</th>
						<th>Opciones</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${subcategorias}" var="subcategoria">
						<tr style="color:#0B0B61;">
							<td>${subcategoria.nombre }</td>
							<td>${subcategoria.tipoProyecto.nombre}</td>
							 <td>	
							<c:if test="${updatesubC}">	
									<a class="modal-trigger" href="#-${subcategoria.idSubcategoria}"><i class="fa fa-pencil" aria-hidden="true"></i></a>&nbsp;
							</c:if>
							<c:if test="${deletesubC}">		
									<a class="" href="/sgp/subcategorias/delete/${subcategoria.idSubcategoria}" data-toggle="modal"data-target="#" onclick="return confirmDel('${subcategoria.idSubcategoria}');" id="resultado"><i class="fa fa-trash" aria-hidden="true"></i></a>
							</c:if>
							</td>				
					
						</tr>
					</c:forEach>
				</tbody>
			</table>
</c:if>		
	</div>
</div>




<!-- /.col-lg-12 -->

<div id="agregar" class="modal white darken-4">
	<div class="modal-header"></div>
	<div class="modal-content">
		<form:form method="post" modelAttribute="subcategoriaForm"
			action="/sgp/subcategorias/add" id="idSubcategoria" autocomplete="off" accept-charset="ISO-8859-1">

			<div class="row">
				<div class="input-field col s6">
					<form:input path="nombre" class="form-control" placeholder="Nombre"
						type="text" id="nombre" value="${subcategoria.nombre}" />

				</div>
				
			</div>
			
		

			<div class="row">
				<div class="input-field col s12">
					<form:select path="idTipoProyecto" class="form-control" required="true">
						<option value="" disabled selected>Seleccione un tipo de Proyecto</option>
						<c:forEach items="${tipoProyectos}" var="p">
							<c:choose>
								<c:when test="${subcategoria.tipoProyecto.idTipoProyecto == p.idTipoProyecto}">
									<form:option value="${p.idTipoProyecto }" label="${p.nombre}"
										selected="true" />
								</c:when>
								<c:otherwise>
									<form:option value="${p.idTipoProyecto }" label="${p.nombre}" />
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</form:select>
				</div>
			</div>
				
					<form:hidden path="idSubcategoria" value="${subcategoria.idSubcategoria}" />

			<div class="center">
				<form:button type="submit"
					class=" btn green modal-actionwaves-effect waves-light white-text" onclick="toast();">
					<i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar </form:button>

				<form:button href="#!"
					class=" btn red modal-action modal-close waves-effect waves-light white-text">
					<i class="fa fa-reply" aria-hidden="true"></i> Cerrar </form:button>
			</div>
		</form:form>
	</div>
</div>




<c:forEach items="${subcategorias}" var="subcategoria">
<div id="-${subcategoria.idSubcategoria}" class="modal white darken-4">
	<div class="modal-header"></div>
	<div class="modal-content">
			<form:form method="post" modelAttribute="subcategoriaForm"
			action="/sgp/subcategorias/add" id="idSubcategoria" autocomplete="off" accept-charset="ISO-8859-1">

			<div class="row">
				<div class="input-field col s6">
					<form:input path="nombre" class="form-control" placeholder="Nombre"
						type="text" id="nombre" value="${subcategoria.nombre}" />

				</div>
				
			</div>
			
		

			<div class="row">
				<div class="input-field col s12">
					<form:select path="idTipoProyecto" class="form-control" required="true">
						<option value="" disabled selected>Seleccione un tipo de Proyecto</option>
						<c:forEach items="${tipoProyectos}" var="p">
							<c:choose>
								<c:when test="${subcategoria.tipoProyecto.idTipoProyecto == p.idTipoProyecto}">
									<form:option value="${p.idTipoProyecto }" label="${p.nombre}"
										selected="true" />
								</c:when>
								<c:otherwise>
									<form:option value="${p.idTipoProyecto }" label="${p.nombre}" />
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</form:select>
				</div>
			</div>
				
					<form:hidden path="idSubcategoria" value="${subcategoria.idSubcategoria}" />

			<div class="center">
				<form:button type="submit"
					class=" btn green modal-actionwaves-effect waves-light white-text" onclick="toast();">
					<i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar </form:button>

				<form:button href="#!"
					class=" btn red modal-action modal-close waves-effect waves-light white-text">
					<i class="fa fa-reply" aria-hidden="true"></i> Cerrar </form:button>
			</div>
		</form:form>
	</div>
</div>
</c:forEach>

<script>
function confirmDel(){
	  var agree=confirm("�Realmente desea eliminarlo? ");
	  if (agree) return true ;
	  return false;
	}

</script>


<script>

    document.getElementById("fechaact1").disabled = true;
    document.getElementById("fechaact2").disabled = true;

</script>

