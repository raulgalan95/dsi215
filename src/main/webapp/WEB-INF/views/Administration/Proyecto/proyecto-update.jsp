<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<div id="-${proyecto.codProyecto}" class="card-panel hoverable" style="margin-bottom:15%;">
	
	<div class="container">
<form:form method="post" modelAttribute="proyectoForm"
			action="/sgp/proyectos/add" id="registro" autocomplete="off" accept-charset="ISO-8859-1">
  
						
			<div class="row">
			<form class="col s12">
			 
			 <fieldset style="border-radius:15px;">
			 <legend><h5 style="text-align: center; "> Datos Generales</h5></legend>
			
				<div class="input-field col s12">
					
			
			<div class="row">
			<div class="input-field col s6">
			<form:select path="codSolicitud" id="form-proyecto" class="form-control" required="required">
						<option value="${proyecto.solicitud.codSolicitud}" disabled selected>${proyecto.solicitud.titulo}</option>
						<c:forEach items="${solicitud}" var="p">
							<c:choose>
								<c:when test="${proyecto.solicitud.codSolicitud == p.codSolicitud}">
									<form:option value="${p.codSolicitud }" label="${p.titulo}"
										selected="true" />
								</c:when>
								<c:otherwise>
									<form:option value="${p.codSolicitud}" label="${p.titulo}" var="selec"/>
								</c:otherwise>
							</c:choose>
			
						</c:forEach>	
			</form:select>
			
			<div class="row">
				<div class="input-field col s6">
				<label for="subcategoria">Seleccione una Subcategoria (Opcional)</label>
					<form:select path="idSubcategoria" class="form-control" required="true">
						<option value="" disabled selected></option>
						<c:forEach items="${subcategorias}" var="p">
							<c:choose>
								<c:when test="${proyecto.subcategoria.idSubcategoria == p.idSubcategoria}">
									<form:option value="${p.idSubcategoria }" label="${p.nombre}"
										selected="true" />
								</c:when>
								<c:otherwise>
									<form:option value="${p.idSubcategoria }" label="${p.nombre}" />
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</form:select>
				</div>
			</div>
			
			<label for="form-proyecto">Proyecto</label>		
					</div>
					<div class="input-field col s6">
			<form:select path="ambitoImpacto" id="ambitoImpacto" class="form-control" value="${proyecto.ambitoImpacto }" required="required">
+				
+				<option value="local">Local</option>
+				<option value="interno">Interno</option>
+				<option value="regional">Regional</option>
+				<option value="nacional">Nacional</option>
+				<option value="internacional">Internacional</option>			
+		    </form:select>
<label for="ambitoImpacto"> Ambito de impacto</label>
			</div>
	</div>
				<div class="row align-center">
				<div class="input-field col s12">
					<form:input path="objeitovG" class="form-control" placeholder="Objetivo General."
						type="text" id="objeitovG" value="${proyecto.objeitovG }" required="required"/>
						<label for="ObjeitovG">Objetivo General</label>
				</div>
			</div>
			
			<div class="row">
				<div class="input-field col s12">
					<form:input path="objetivoE1" class="form-control" placeholder="Objetivo Especifico"
						type="text" id="objetivoE1" value="${proyecto.objetivoE1 }" required="required"/>
						<label for="objetivoE1">Primer Objetivo Especifico</label>
				</div>
			
			
			
				<div class="input-field col s12">
					<form:input path="objetivoE2" class="form-control" placeholder="Objetivo Especifico"
						type="text" id="objetivoE2" value="${proyecto.objetivoE2 }" required="required"/>
					<label for="objetivoE2">Segundo Objetivo Especifico</label>
				</div>
			
			
				<div class="input-field col s12">
					<form:input path="objetivoE3" class="form-control" placeholder="Objetivo Especifico"
						type="text" id="objetivoE3" value="${proyecto.objetivoE3 }" required="required"/>
				<label for="objetivoE3">Tercer Objetivo Especifico</label>
				</div>
			
			
				<div class="input-field col s12">
					<form:input path="objetivoE4" class="form-control" placeholder="Objetivo Especifico"
						type="text" id="objetivoE4" value="${proyecto.objetivoE4 }" />
						<label for="objetivoE4">Cuarto Objetivo Especifico- Opcional</label>
				</div>
			
			
				<div class="input-field col s12">
					<form:input path="objetivoE5" class="form-control" placeholder="Objetivo Especifico"
						type="text" id="objetivoE5" value="${proyecto.objetivoE5 }" />
						<label for="objetivoE5">Quinto Objetivo Especifico - Opcional</label>
					</div>
			
			</div>
			</div>
			</fieldset>
</div>
			<div>
            <fieldset style="border-radius:15px;">
				<legend><h5 class="align-center"> Tiempo y financiamiento</h5></legend>
				
			<div class="row">
				<div class="input-field col s6">
				 Desde
					<form:input path="fechaInicio" class="form-control" placeholder="fecha"
						type="date" id="fechaInicio" value="${proyecto.fechaInicio}" required="required"/>
						
				</div>
				<div class="input-field col s6">
				Hasta
						<form:input path="fechaFin" class="form-control" placeholder="fecha"
						type="date" id="fechaFin" value="${proyecto.fechaFin}" required="required"/>
						
				</div>
			</div>
			
			
			
			<input type="button" class=" btn blue modal-actionwaves-effect waves-light white-text" value="Calcular Duracion del proyecto" onclick="calcularDias();">
			<div class="row">
				
			<div  class="input-field col s12">
			<form:input path="duracion" class="form-control" placeholder="Duracion"
						type="text"   value="${proyecto.duracion}" required="required" id="resultado"/>
				</div>
				</div>
			<div>
			<label for="tipofinanciamiento">Tipo de financiamiento</label>
			<form:select path="tipoFinanciamiento" id="tipofinanciamiento" class="form-control" value="${proyecto.tipoFinanciamiento }" required="true">
				<option value="donacion">Donacion</option>
				<option value="aporte">Aporte</option>
			</form:select>
			
				</div>
			<div class="row">
				<div class="input-field col s12">
					<form:input path="patrocinadores" id="patrocinadores" value=" ${ proyecto.patrocinadores } " class="form-control" placeholder="Patrocinadores"
					rows="8" cols="80" required="required"  style="font-size:15px;"/>
					<label for="patrocinadores">Patrocinadores</label>
				</div>
			</div>
			
			<div class="row">
				<div class="input-field col s12">
					<form:input path="montoAprobado" class="form-control" placeholder="MontoAprobado."
						type="number" id="montoAprobado" value="${proyecto.montoAprobado }" required="required"/>
						<label for="montoAprobado">Monto Aprobado</label>
				</div>
			</div>
			<div>
			<label for="origFondos">Origen de los Fondos</label>
			<form:select path="origFondos" id="origFondos" class="form-control" value="${proyecto.origFondos}" required="required">
				<option value="FondosPropios">Fondos propios</option>
				<option value="InstPublica">Institucion publica</option>
				<option value="InstPrivada">Institucion privada</option>
				<option value="ONG">ONG</option>
				<option value="Cooperacion">Cooperacion internacional</option>
				</form:select>
				
			</div>
			</fieldset>
			</div>
			
			
			<form:hidden path="codProyecto" value="${proyecto.codProyecto}" />
			
			
					
		<fieldset style="border-radius:15px;">
					 <legend><h5 class="aling-center"> Protocolo del Proyecto</h5></legend>
					
					<div class="row">
				<div class="input-field col s12">
				<form:input path="resumen" class="form-control" placeholder="Resumen"
						type="text" id="resumen" value="${proyecto.resumen }" required="required"/>
						<label for="resumen">Resumen</label>
				
				</div>
			</div>
			
			<div class="row">
				<div class="input-field col s12">
					<form:input path="planteamiento" id="planteamiento" class="form-control" placeholder="Planteamiento del problema"
						rows="8" cols="80" value="${proyecto.planteamiento }" required="required" style="font-size:15px;"/>
				<label for="planteamiento">Planteamiento</label>
				</div>
			</div>
			
			
			<div class="row">
				<div class="input-field col s12">
					<form:input path="antecedentes"  id="antecedentes" class="form-control" placeholder="Antecedentes del proyecto"
						rows="8" cols="80"  value="${proyecto.antecedentes }" required="required"  style="font-size:15px;"/>
				<label for="antecedentes">Antecedentes</label>
				</div>
			</div>
				
				
			
					
					<div class="row">
				<div class="input-field col s12">
					<form:input path="metodologia" id="metodologia" class="form-control" placeholder="Metodologia o Tecnicas"
					rows="8" cols="80"	 value="${proyecto.metodologia }" required="required"  style="font-size:15px;"/>
				<label for="metodologia">Metodologia</label>
				</div>
			</div>

			<div class="row">
				<div class="input-field col s12">
					<form:input path="medidaSostenibilidad" class="form-control" placeholder="Medida de sostenibilidad"
						type="text" id="ms" value="${proyecto.medidaSostenibilidad }" required="required"/>
				<label for="ms">Medidas de sostenibilidad</label>
				</div>
			</div>			
				
						<div>
      	<c:if test="${proyecto.isInvolucracion()}">
        	<input type="checkbox" id="selection" name="selection" value="si" checked="checked"/>
       		<label for="selection">Estudiantes</label>
    	</c:if>
    	<c:if test="${proyecto.isInvolucracion()==false}">
        	<input type="checkbox" id="selection" name="selection" value="si"/>
       		<label for="selection">Estudiantes</label>
    	</c:if>
    	
			</div>
			</fieldset>
				</div>
			
	
			<div class="center">
				<form:button type="submit"
					class=" btn green modal-actionwaves-effect waves-light white-text" onclick="toast();">
					<i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar </form:button>

				<form:button href="#!" class=" btn red modal-action modal-close waves-effect waves-light white-text">
					<i class="fa fa-reply" aria-hidden="true"></i> Cerrar </form:button>
			</div>
		</form:form>
	</div>

