<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>



<div id="contenido" class="card-panel hoverable" style="margin-bottom:15%;">

<c:if test="${createtipoS}">	
		<a class="waves-effect waves-light btn-floating modal-trigger green" href="/sgp/proyecto/add/"><i class="fa fa-plus-circle" aria-hidden="true"></i>Agregar</a>&nbsp;&nbsp;
		<a class="waves-effect waves-light btn-floating modal-trigger red" href="/sgp/vistasPDF"><i class="large material-icons" aria-hidden="true">picture_as_pdf</i></a>&nbsp;&nbsp;

</c:if>



		
		<hr>	
	 	<div class="container">	
<c:if test="${readtipoS}">	
			<table id="example" class="display hover cell-border"  cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>Nombre</th>
						<th>Responsable</th>
						<th>Tipo Proyecto</th>
						<th>Duracion <br>[dias]</th>
						<th>Opciones</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${proyectos}" var="proyecto">
						<tr style="color:#0B0B61;">
							<td>${proyecto.solicitud.titulo }</td>
							 <td>${proyecto.solicitud.usuario.username }</td>
							 <td>${proyecto.solicitud.tipoProyecto.nombre }</td>
							 <td>${proyecto.duracion }</td>
						
		
							<td style="width:20%;">
						
							<c:if test="${updatetipoS}">	
									<a class="modal-trigger tooltipped" href="/sgp/proyectos/update/${proyecto.codProyecto}" data-position="left" data-tooltip="Actualizar"><i class="fa fa-pencil" aria-hidden="true"></i></a>&nbsp;
							</c:if>
							<c:if test="${deletetipoS}">		
							<a class="tooltipped" href="/sgp/proyectos/delete/${proyecto.codProyecto}" data-toggle="modal"data-target="#" onclick="return confirmDel('${proyecto.codProyecto}');" data-position="bottom" data-tooltip="Eliminar"><i class="fa fa-trash" aria-hidden="true"></i></a>&nbsp;
							<a class="tooltipped" href="/sgp/matriz/${proyecto.codProyecto}" data-position="bottom" data-tooltip="Matriz"><i class="fa fa-th-large red-text" aria-hidden="true"></i></a>&nbsp;
							<a class="tooltipped" href="/sgp/pi/${proyecto.codProyecto}" data-position="right" data-tooltip="Indicador"><i class="fa fa-line-chart green-text"></i></a>&nbsp;
							<a class="tooltipped" href="/sgp/actividad/${proyecto.codProyecto}" data-position="right" data-tooltip="Actividades"><i class="fa fa-folder green-text"></i></a>
									
							</c:if>						
						</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
</c:if>		
	</div>
</div>



<!-- /.col-lg-12 -->








<script  type="text/javascript">
function calcularDias()
{
	var fechaInicial=document.getElementById("fechaInicio").value;
	var fechaFinal=document.getElementById("fechaFin").value;
	var resultado="";
	
	
	inicial=fechaInicial.split("-");
	finals=fechaFinal.split("-");
	// obtenemos las fechas en milisegundos
	var dateStart=new Date(inicial[0],(inicial[1]-1),inicial[2]);
	var dateEnd=new Date(finals[0],(finals[1]-1),finals[2]);
        if(dateStart<dateEnd)
        {
			// la diferencia entre las dos fechas, la dividimos entre 86400 segundos
			// que tiene un dia, y posteriormente entre 1000 ya que estamos
			// trabajando con milisegundos.
			resultado=(((dateEnd-dateStart)/86400)/1000);
		}else{
			resultado="La fecha inicial es posterior a la fecha final";
		}
        
	
	document.getElementById("resultado").value=resultado;
}

</script>


<script>

function Borrar(codProyecto)
{

 var resul = confirm('�Desea borrar el proyecto seleccionado?');
 if(resul=true)
	 {
	   location.href="/sgp/proyectos/delete/"+codProyecto;
	 }
 else (resul=false)
 {
	 location.href="/sgp/proyectos";
	}
 
} 
</script>



<script>
document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.tooltipped');
    var instances = M.Tooltip.init(elems, options);
  });

  // Or with jQuery

  $(document).ready(function(){
    $('.tooltipped').tooltip();
  });
  function confirmDel(){
	  var agree=confirm("�Realmente desea eliminarlo? ");
	  if (agree) return true ;
	  return false;
	} 
  
</script>


<!--  <script type="text/javascript">
$(document).ready(function(){
	$('#example').DataTable({
		dom:'Bfrtip',
		buttons:[
			'copy', 'csv', 'excel', 'pdf', 'print'
		]
	});
});

</script>-->




