<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

 <!--Load the AJAX API-->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script type="text/javascript">


google.charts.load('current', {packages: ['corechart', 'bar']});
google.charts.setOnLoadCallback(drawBasic);

    function drawBasic()
    {
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Nombre de Proyectos');
    data.addColumn('number', 'Duraci�n');

    data.addRows(${duracion});
          
      var options = {
    	width: 1000,
    	height: 400,
    	legend: { position: 'button', textStyle: {fontSize: 5} },
        title: 'Duaci�n de Proyectos',
        colors: ['#e0440e', '#e6693e', '#ec8f6e', '#f3b49f', '#f6c7b6'],
        
        hAxis: {
          title: 'Proyectos'
     
        },
        vAxis: {
          title: 'D�as'
        }
      };

      var chart = new google.visualization.ColumnChart(
        document.getElementById('chart_div'));

      chart.draw(data, options);
    }



    </script>
    </head>
    <body>
    <div id="chart_div"></div>
    </body>
    </html>             